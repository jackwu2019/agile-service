package io.choerodon.agile.api.vo;

/**
 * @author shinan.chen
 * @since 2019/5/27
 */
public class IssueIdSprintIdVO {

    private Long issueId;
    private Long sprintId;
    private Long assigneeId;

    public Long getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Long assigneeId) {
        this.assigneeId = assigneeId;
    }

    public Long getIssueId() {
        return issueId;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }

    public Long getSprintId() {
        return sprintId;
    }

    public void setSprintId(Long sprintId) {
        this.sprintId = sprintId;
    }
}

